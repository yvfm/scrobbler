#!/usr/bin/env bash

set -eux

for X in *.php ; do
  php -l "$X"
done

dest="$1"

install -m0444 update.php "$dest/"
install -m0444 config.php "$dest/"
install -m0444 robots.txt "$dest/"
