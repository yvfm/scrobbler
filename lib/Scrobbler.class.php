<?php

/*
 * Copyright (c) 2021 Phillip Smith. All rights reserved.
 * This work is licensed under the terms of the MIT license.  
 */

declare(strict_types=1);

class Scrobbler {

  public $trackData = [];
  public $errMsg;

  private $fieldMap = [
      'artist' => 'artist',
      'title'  => 'title',
      'year'   => 'year',
      'path'   => 'path',
      'fpath'  => 'path',
      'genre'  => 'genre',
      'length' => 'length',
      'len'    => 'length',
  ];

  private function startsWith($string, $startString) {
    $len = strlen($startString);
    return (substr($string, 0, $len) === $startString);
  }

  /*
   * Extracts known fields from GET/POST into an array. Field names are mapped where the client may supply a name
   * other than what we want (eg, len => length).
   */
  public function loadTrackData() {
    $method = $_SERVER['REQUEST_METHOD'];

    // Loop over our fieldMap and extract from GET/POST as appropriate into a temporary array
    $trackData = [];
    foreach ($this->fieldMap as $sourceKey => $targetKey) {
      switch ($method) {
      case 'GET':
        if (array_key_exists($sourceKey, $_GET))
          $trackData[$targetKey] = $_GET[$sourceKey];
        break;
      case 'POST':
        if (array_key_exists($sourceKey, $_POST))
          $trackData[$targetKey] = $_POST[$sourceKey];
        break;
      }
    }

    // RadioBoss will send "(Unavailable)" in the artist and path fields when it *stops* playing, so in this case
    // we don't want to return any "track data"
    if (array_key_exists('artist', $trackData) and array_key_exists('fpath', $trackData))
      if (trim($trackData['artist']) == '(Unavailable)' and trim($this->trackData['fpath']) == '(Unavailable)')
        return null;

    $this->trackData = $trackData;
    return $this->trackData;
  }

  /*
   * Formats metadata about a music track into a "pretty" string
   */
  public function formatTrackText() {
    // Convert HH:MM:SS track length to seconds integer
    $trackSeconds = null; 
    if (array_key_exists('length', $this->trackData)) {
      sscanf($this->trackData['length'], "%d:%d:%d", $hours, $minutes, $seconds);
      if ($seconds == null) {
        $seconds = $minutes;
	$minutes = $hours;
	$hours = null;
      }
      $trackSeconds = isset($hours) ?
        ($hours * 3600) + ($minutes * 60) + $seconds :
	($minutes * 60) + $seconds;
    }
    $this->trackData['seconds'] = $trackSeconds;

    // Sometimes it's just not worth updating the metadata
    $this->errMsg = null;
    if (!array_key_exists('title', $this->trackData) or trim($this->trackData['title']) == '')
      $this->errMsg = 'No Title.';
    if (array_key_exists('artist', $this->trackData) and in_array($this->trackData['artist'], ['CSA', 'Sponsor']))
      $this->errMsg = sprintf('Artist is %s (blacklisted)', $this->trackData['artist']);
    if ($trackSeconds !== null and $trackSeconds < 60)
      $this->errMsg = 'Track too short.';

    // If the title is all lowercase, then it's probably not a title that's been properly set
#    if (strtolower($trackData['title']) == $this->trackData['title'])
#      $this->errMsg = 'Title is all lower-case; Probably not a properly maintained tag.';

    // Some track data is just unmaintained hot garbage.
    // Often the "Title" is combined Artist and Title, formatted like this:
    //   Beach Baby - (First Class)
    //   I Found Someone - (Cher)
    // Check for this pattern using a regex and skip if it looks like one of these tracks
    if (array_key_exists('title', $this->trackData) and preg_match('/ - \(.$\)$/', trim($this->trackData['title'])))
      $this->errMsg = 'Matched combined Artist+Title pattern in Title field.';

    // If the tag contains "Official Video" then it's possibly a rip from YouTube :|
    if (array_key_exists('title', $this->trackData) and preg_match('/Official Video/', trim($this->trackData['title'])))
      $this->errMsg = 'Possible YouTube Ripped Video';

    // Another possible rip from YouTube :|
    // "Rockwell - Somebody's Watching Me (Lyrics)"
    // "Deirdre Browne – Come As You Are (Music Sheets, Chords, & Lyrics)"
    if (array_key_exists('title', $this->trackData) and preg_match('/\(.*Lyrics\)/i', trim($this->trackData['title'])))
      $this->errMsg = 'Possible YouTube Ripped Video';

    // If we found an error, return null
    if ($this->errMsg !== null)
      return null;

    if (trim($this->trackData['artist']) == 'Unknown' or trim($this->trackData['artist']) == '') {
      // Just the title (no artist)
      return $this->trackData['title'];
    } else {
      // Artist and Title
      return sprintf('%s by %s', $this->trackData['title'], $this->trackData['artist']);
    }
  }

}
