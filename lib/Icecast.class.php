<?php

/*
 * Copyright (c) 2021 Phillip Smith. All rights reserved.
 * This work is licensed under the terms of the MIT license.  
 */

declare(strict_types=1);

class icServer {

  private $username, $password, $hostname, $mount;
  private $port;
  private $debug;

  public function __construct($hostname, $port) {
    $this->hostname = $hostname;
    $this->port = $port;
    $this->debug = false;
  }

  public function __get($name) {
    if ($name == 'password' )
      throw new InvalidArgumentException('Password is not getable');

    return $this->$name;
  }

  public function __set($name, $value) {
    return ($this->$name = $value);
  }

  private function _queryIcecastAdmin($query) {
    if (!$this->testReadyForUse())
      return false;

    $url = sprintf('https://%s:%s/%s', $this->hostname, $this->port, ltrim($query,'/'));
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return response instead of printing it
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERPWD, "$this->username:$this->password");
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_VERBOSE, $this->debug);
    $data = curl_exec($ch);
    curl_close($ch);
    if ($data)
      return simplexml_load_string($data);
    else
      return $data; // wat?
  }

  public function testReadyForUse() {
    if (!$this->hostname)
      return false;
    if (!$this->username)
      return false;
    if (!$this->password)
      return false;
    if (!$this->mount)
      return false;

    return true;
  }

  public function updateNowPlaying($text) {
    $text = preg_replace('/\s+/', '+', $text);
    $q = sprintf('/admin/metadata?mount=%s&mode=updinfo&song=%s', $this->mount, $text);
    return $this->_queryIcecastAdmin($q);
  }

  public function listClients() {
    return $this->_queryIcecastAdmin('/admin/listclients?mount='.$this->mount);
  }

  public function killClient($id) {
    return $this->_queryIcecastAdmin(sprintf('/admin/killclient?mount=%s&id=%s', $this->mount, $id));
  }

  public function getListenerCount() {
    $xmlData = $this->listClients();
    if ($xmlData)
      return $xmlData->source->Listeners;
    else
      return false;
  }

}
