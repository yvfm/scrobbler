<?php

/*
 * Copyright (c) 2021 Phillip Smith. All rights reserved.
 * This work is licensed under the terms of the MIT license.  
 */

declare(strict_types=1);

class Logger {

  private $logLevels = [
    0 => 'Error',
    1 => 'Warning',
    2 => 'Info',
    3 => 'Debug',
  ];

  private $logFilename;
  private $logLevel;

  public function __construct($filename, $logLevel=2) {
    $this->logFilename = $filename;
    $this->logLevel = $logLevel;
  }

  /*
   * Write a log to file.
   * $msgLevel is positie integer representing log priority
   */
  public function write($msgLevel, $msgText) {
    // Return early if log level higher than configured
    if ($msgLevel > $this->logLevel)
      return true;

    // Format the log
    $logMessage = sprintf("%s\t%s\t%s",
      date('Y-m-d H:i:s'),
      $this->logLevels[$msgLevel],
      $msgText
    );

    // Write it to file
    file_put_contents($this->logFilename, $logMessage.PHP_EOL , FILE_APPEND | LOCK_EX);

    return true;
  }

}
