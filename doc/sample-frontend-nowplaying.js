<!-- begin update track title code -->
<div id="now_playing"></div>

<script type="text/javascript">
var getJSON = function(url, callback) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', url, true);
  xhr.responseType = 'json';
  xhr.onload = function() {
    var status = xhr.status;
    if (status === 200) {
      callback(null, xhr.response);
    } else {
      callback(status, xhr.response);
    }
  };
  xhr.send();
};

function UpdateTitle() {
  getJSON('https://icecast.example.com:8443/status-json.xsl', function(err, data) {
    if (err !== null) {
      console.log('Failed to fetch JSON: ' + err);
    } else {
      displayText = data.icestats.source[0].title;
      if (typeof displayText !== 'undefined') {
        document.getElementById('now_playing').innerHTML = displayText;
      } else {
        console.log(data.icestats);
      }
    }
  });
}

setInterval("UpdateTitle()", 15000);
UpdateTitle();
</script>
<!-- end update track title code -->
