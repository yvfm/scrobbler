<?php

/*
 * IP Addresses that are permitted to access
 */
$validClients = [
  '127.0.0.1',	// localhost ipv4
  '::1',	// localhost ipv6
];

/*
 * Log Levels:
 *   0 = Error
 *   1 = Warning
 *   2 = Info
 *   3 = Debug
 */
$logConfig = [
  'level'    => 3,
  'filename' => '/tmp/update.log',
];

$metadataPrefix = null;

/*
 * RDS Device Configuration
 */
$dRDSHost = null;
$dRDSPort = null;
$dRDSPass = null;	// Optional
$dRDSDefaultText = $metadataPrefix;
$dRDSTimeoutSecs = 3;
$dRDSMaxLength = 64;

/*
 * Icecast Configuration
 */
$icHost = null;
$icPort = null;
$icUser = null;
$icPass = null;

// stream mountpoints to update metadata on
$icMountpoints = [
];
