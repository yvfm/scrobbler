<?php

/*
 * Copyright (c) 2021 Phillip Smith. All rights reserved.
 * This work is licensed under the terms of the MIT license.  
 */

declare(strict_types=1);

if (php_sapi_name() === 'cli') {
  echo "This script cannot be run from the command line.\n";
  exit(1);
}

// Load our helper libraries
require('lib/Scrobbler.class.php');
require('lib/Logger.class.php');
require('lib/Icecast.class.php');
require('lib/Telnet.class.php');
require('lib/Fuckless.class.php');

// Load default and local config files
require('config.php');
include('config.local.php');

/*
 * Load a random generic text item instead of track data
 */
function getGenericText() {
  $srcFiles = glob('generics/*.txt');
  $srcFile = $srcFiles[array_rand($srcFiles)];
  $f_contents = file($srcFile);
  return $f_contents[0];
}

/*
 * Extremely simple check of IP address against whitelist of permitted IP addresses
 */
function checkClientIP($clientIP) {
  global $validClients;  // from the config file(s)
  foreach ($validClients as $validClient) {
    if ($clientIP == $validClient)
      return true;
    // Next, a lazy and incomplete (but quick) way of doing CIDR comparison
    if (startsWith($clientIP, $validClient))
      return true;
  }
  return false;
}

/*
 * String comparison function to test if a string starts with a given substring.
 */
function startsWith($string, $startString) {
  $len = strlen($startString);
  return (substr($string, 0, $len) === $startString);
}

#######################################################################################################################
$log = new Logger($logConfig['filename'], $logConfig['level']);
$log->write(3, "========================================================");
$log->write(2, sprintf("Starting new %s request from %s.", $_SERVER['REQUEST_METHOD'], $_SERVER['REMOTE_ADDR']));

// Validate request is from an authorized client
$clientIP = $_SERVER['REMOTE_ADDR'];
$isTrustedIP = checkClientIP($clientIP);
if (!$isTrustedIP) {
  $log->write(1, sprintf('Client %s is Unauthorized', $clientIP));
  http_response_code(403); // forbidden
  exit(0);
}

// Get the data from the client, save to disk and format it nicely.
$scrobbler = new Scrobbler;
$trackData = $scrobbler->loadTrackData();
if ($trackData !== null) {
  $niceTrackText = $scrobbler->formatTrackText();
  $log->write(3, "trackdata: ".print_r($scrobbler->trackData, true));
} else {
  $log->write(3, "loadTrackData() returned null (probably playout system reporting a 'stop' event)");
  $niceTrackText = null;
}
if ($niceTrackText !== null) {
  $log->write(3, '$niceTrackText: '.$niceTrackText);
} else {
  $log->write(2, 'No nice track text because: '.$scrobbler->errMsg);
  // Randomly use a generic text instead (80% of the time
  if (mt_rand(0,100) > 20) {
    $log->write(2, 'Selecting a random generic text instead');
    $niceTrackText = getGenericText();
  }
}

// Return to the client before we start updating other systems so we don't block the client.
http_response_code(204); // ok (no data)
fastcgi_finish_request();

#######################################################################################################################
### Scrub the data for each destination ###
try {
  // If libScrobbler didn't return a string for us, reset to the 'default' text
  $dRDSText = ($niceTrackText ?: $dRDSDefaultText);
  $icText = ($niceTrackText ?: $metadataPrefix);

  // Condense multiple spaces
  $dRDSText = preg_replace('/\s+/', ' ', $dRDSText);
  $icText = preg_replace('/\s+/', ' ', $icText);
} catch (exception $e) {
  $log->write(0, 'Exception while scrubbing input data: '.$e->getMessage());
  exit(1);
}

### Update RDS Device ###
# This is currently written to support the Deva DB64-FM, with password enabled.
# We don't have anything else, so this is very specific currently.
$retryCount = 1;
while ($retryCount <= 3) {
  try {
    if ($dRDSHost) {
      $log->write(3, 'RDS Host is '.$dRDSHost);

      // Strip non-alphanumeric characters and basic punctuation
      $dRDSText = preg_replace("/[^A-Za-z0-9 \.\,\-\+\(\)\[\]:=%$#@!&]/", '', $dRDSText);

      // Limit to 64 characters
      if (strlen($dRDSText) > $dRDSMaxLength) {
        $dRDSText = wordwrap($dRDSText, $dRDSMaxLenth);
        $dRDSText = substr($dRDSText, 0, strpos($dRDSText, "\n"));
      }

      // Basic check for offensive language
      if (Fuckless::hasBadWords($dRDSText) !== false) {
        $log->write(1, 'Reverting to default text because bad words found in dynamic RDS Text: '.$dRDSText);
        $dRDSText = $dRDSDefaultText;
      }

      // Connect and Authenticate
      $log->write(2, sprintf('RDS Text update (attempt %d): %s', $retryCount, $dRDSText));
      $dRDS = new Telnet($dRDSHost, $dRDSPort, $dRDSTimeoutSecs, "PASS?");
      $dRDS->setPrompt('PASSOK');
      $dRDS->exec($dRDSPass);
      // Set the DPS and disconnect
      $dRDS->setPrompt('OK');
      $dRDS->exec('TEXT='.$dRDSText);
      $dRDS->exec('DPS=');	// Ensure DPS is always blank - never use DPS!
      $dRDS->disconnect();
    } else {
      $log->write(3, 'Skipping RDS update because no details configured.');
    }
    break;
  } catch (exception $e) {
	  $log->write(0, 'Exception while update RDS: '.$e->getMessage());

    // Increase the retry counter, and sleep for twice the retry count
    $retryCount++;
    sleep($retryCount * 2);
  }
}

### Update Icecast Stream Metadata ###
$retryCount = 1;
while ($retryCount <= 3) {
  try {
    if ($icHost) {
      $log->write(3, 'Icecast Host is '.$icHost);
      $log->write(2, 'Icecast Text: '.$icText);
      $icAPI = new icServer($icHost, $icPort);
      $icAPI->username = $icUser;
      $icAPI->password = $icPass;
      foreach ($icMountpoints as $mp) {
        $log->write(3, sprintf('Updating Icecast Mountpoint "%s"', $mp));
        $icAPI->mount = $mp;
        $icAPI->updateNowPlaying($icText);
      }
    } else {
      $log->write(3, 'Icecast not configured; skipping');
    }
    break;
  } catch (exception $e) {
    $log->write(0, 'Exception updating Icecast Data: '.$e->getMessage());

    // Increase the retry counter, and sleep for twice the retry count
    $retryCount++;
    sleep($retryCount * 2);
  }
}

exit(0);
